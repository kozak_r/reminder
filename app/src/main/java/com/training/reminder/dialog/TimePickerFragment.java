package com.training.reminder.dialog;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import com.training.reminder.util.DateTimeUtils;

import java.util.Calendar;
import java.util.HashMap;

/**
 * @author Roman Kozak (21-Apr-2018)
 */
public class TimePickerFragment extends PickerFragment implements
        TimePickerDialog.OnTimeSetListener {

    static TimePickerFragment newInstance(HashMap<String, Object> argsMap) {
        TimePickerFragment fragment = new TimePickerFragment();
        return (TimePickerFragment) PickerFragment.newInstance(fragment, argsMap);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        boolean is24HourView = DateFormat.is24HourFormat(getActivity());
        return new TimePickerDialog(getActivity(), this, hour, minute, is24HourView);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        editText.setText(DateTimeUtils.getTime(calendar.getTimeInMillis()));
    }

}
