package com.training.reminder.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.training.reminder.R;
import com.training.reminder.alarm.AlarmHelper;
import com.training.reminder.model.ModelTask;

import java.util.Calendar;
import java.util.HashMap;

/**
 * @author Roman Kozak (21-Apr-2018)
 */
public class AddingTaskDialogFragment extends DialogFragment {

    private AddingTskListener addingTaskListener;

    public interface AddingTskListener {
        void onTaskAdded(ModelTask task);
        void onTaskAddingCancel();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            addingTaskListener = (AddingTskListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement AddingTaskListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_title);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View container = inflater.inflate(R.layout.dialog_task, null);


        final TextInputLayout tilTitle = container.findViewById(R.id.tilDialogTaskTitle);
        tilTitle.setHint(getResources().getString(R.string.task_title));
        final EditText etTitle = tilTitle.getEditText();

        final ModelTask task = new ModelTask();

        ArrayAdapter<String> priorityAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, ModelTask.PRIORITY_LEVELS);
        Spinner spPriority = container.findViewById(R.id.spDialogTaskPriority);
        spPriority.setAdapter(priorityAdapter);
        spPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                task.setPriority(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);

        TextInputLayout tilDate = container.findViewById(R.id.tilDialogTaskDate);
        tilDate.setHint(getResources().getString(R.string.task_date));
        final EditText etDate = tilDate.getEditText();
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etDate.length() == 0) {
                    etDate.setText(" ");
                }

                HashMap<String, Object> argsMap = new HashMap<>();
                argsMap.put(PickerFragment.EDIT_TEXT_ELEMENT_NAME, etDate);
                argsMap.put(PickerFragment.CALENDAR_ELEMENT_NAME, calendar);

                DialogFragment datePickerFragment;
                datePickerFragment = DatePickerFragment.newInstance(argsMap);
                datePickerFragment.show(getFragmentManager(), "DatePickerFragment");
            }
        });

        TextInputLayout tilTime = container.findViewById(R.id.tilDialogTaskTime);
        tilTime.setHint(getResources().getString(R.string.task_time));
        final EditText etTime = tilTime.getEditText();
        etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etTime.length() == 0) {
                    etTime.setText(" ");
                }

                HashMap<String, Object> argsMap = new HashMap<>();
                argsMap.put(PickerFragment.EDIT_TEXT_ELEMENT_NAME, etTime);
                argsMap.put(PickerFragment.CALENDAR_ELEMENT_NAME, calendar);

                DialogFragment timePickerFragment;
                timePickerFragment = TimePickerFragment.newInstance(argsMap);
                timePickerFragment.show(getFragmentManager(), "TimePickerFragment");
            }
        });

        builder.setView(container);

        builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                task.setTitle(etTitle.getText().toString());
                task.setStatus(ModelTask.STATUS_CURRENT);
                if (etDate.length() != 0 || etTime.length() != 0) {
                    task.setDate(calendar.getTimeInMillis());

                    AlarmHelper alarmHelper = AlarmHelper.getInstance();
                    alarmHelper.setAlarm(task);
                }
                task.setStatus(ModelTask.STATUS_CURRENT);
                addingTaskListener.onTaskAdded(task);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addingTaskListener.onTaskAddingCancel();
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if(etTitle == null) {
                    return;
                }
                final Button positiveButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                if(etTitle.length() == 0) {
                    positiveButton.setEnabled(false);
                    tilTitle.setError(getResources().getString(R.string.dialog_error_empty_title));
                }

                etTitle.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            positiveButton.setEnabled(false);
                            tilTitle.setError(getResources().getString(R.string.dialog_error_empty_title));
                        } else {
                            positiveButton.setEnabled(true);
                            tilTitle.setErrorEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
        });
        return alertDialog;
    }
}
