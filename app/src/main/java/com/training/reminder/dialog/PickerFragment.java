package com.training.reminder.dialog;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;

import java.util.Calendar;
import java.util.HashMap;

/**
 * @author Roman Kozak (21-Apr-2018)
 */
public class PickerFragment extends DialogFragment {

    public static final String ARGS_NAME = "element_wrapper";
    public static final String EDIT_TEXT_ELEMENT_NAME = "edit_text";
    public static final String CALENDAR_ELEMENT_NAME = "calendar";

    EditText editText;
    Calendar calendar;

    static PickerFragment newInstance(PickerFragment fragment, HashMap<String, Object> argsMap) {
        Bundle args = new Bundle();
        args.putSerializable(ARGS_NAME, argsMap);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HashMap<String, Object> argsMap = (HashMap<String, Object>) getArguments().get(ARGS_NAME);
        if (argsMap != null) {
            this.editText = (EditText) argsMap.get(EDIT_TEXT_ELEMENT_NAME);
            this.calendar = (Calendar) argsMap.get(CALENDAR_ELEMENT_NAME);
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        editText.setText(null);
    }
}
