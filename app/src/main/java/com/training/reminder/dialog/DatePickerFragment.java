package com.training.reminder.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import com.training.reminder.util.DateTimeUtils;

import java.util.Calendar;
import java.util.HashMap;

/**
 * @author Roman Kozak (21-Apr-2018)
 */
public class DatePickerFragment extends PickerFragment implements
        DatePickerDialog.OnDateSetListener {

    static DatePickerFragment newInstance(HashMap<String, Object> argsMap) {
        DatePickerFragment fragment = new DatePickerFragment();
        return (DatePickerFragment) PickerFragment.newInstance(fragment, argsMap);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        editText.setText(DateTimeUtils.getDate(calendar.getTimeInMillis()));
    }

}
