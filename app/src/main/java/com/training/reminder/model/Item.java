package com.training.reminder.model;

/**
 * @author Roman Kozak (21-Apr-2018)
 */
public interface Item {

    boolean isTask();

}
