package com.training.reminder.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.training.reminder.database.DBHelper;
import com.training.reminder.database.DBQueryManager;
import com.training.reminder.model.ModelTask;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roman Kozak (10-May-2018)
 */
public class AlarmSetter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DBHelper dbHelper = new DBHelper(context);

        AlarmHelper.getInstance().init(context);
        AlarmHelper alarmHelper = AlarmHelper.getInstance();

        List<ModelTask> tasks = new ArrayList<>();
        DBQueryManager dbQueryManager = dbHelper.query();
        tasks.addAll(dbQueryManager.getTasks(
                DBHelper.SELECTION_STATUS + " OR " + DBHelper.SELECTION_STATUS,
                new String[] {
                        Integer.toString(ModelTask.STATUS_CURRENT),
                        Integer.toString(ModelTask.STATUS_OVERDUE)
                },
                DBHelper.TASK_DATE_COLUMN
        ));

        for (ModelTask task : tasks) {
            if (task.getDate() != 0) {
                alarmHelper.setAlarm(task);
            }
        }

    }

}
